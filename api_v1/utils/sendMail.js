const nodemailer = require('nodemailer');
const config = require('config');

let sendMail = function(ctx){
    return new Promise( (resolve, reject) => {
        let transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
              user: config.get("email_username"),
              pass: config.get("email_password")
            }
          });
          
          var mailOptions = {
            from: config.get("email_username"),
            to: ctx.email_to,
            subject: ctx.subject, // 'Todo platform reset password',
            text: ctx.text //'For Todo platform your url to reset password is http://localhost:8000.'
          };
          
          transporter.sendMail(mailOptions, function(error, info){
            if (error) {
              reject(error);
            } else {
              resolve({success: true, msg: `Email sent: ${info.response}`});
            }
          });
    });
}

module.exports = {
    sendMail
}