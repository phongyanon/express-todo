const config = require('config');

module.exports = class Todo {
    constructor() {
        let Query;
        switch(config.get('db_type')){
            case('mysql'):
                Query = require('./database/mysql');
                break;
            case('mongo'):
                Query = require('./database/mongo');
                break;
            case('arango'):
                Query = require('./database/arango');
                break;
        }
        
        this.query = new Query();
    }

    getAllTodo(){
        return new Promise( async (resolve, reject) => {
            let query = this.query;
            try {
                let result = await query.getAllTodo();
                resolve(result);
            } catch (err) {
                reject(err.toString());
            }
        });
    }

    getTodo(ctx){
        return new Promise( async (resolve, reject) => {
            let query = this.query;
            try {
                let result = await query.getTodo(ctx);
                resolve(result);
            } catch (err) {
                reject(err.toString());
            }
        });
    }

    addTodo(ctx){
        return new Promise( async (resolve, reject) => {
            let query = this.query;
            try {
                let result = await query.addTodo(ctx);
                let newTodo = await query.getTodo({id: result.newId});
                resolve({status: result.status, newTodo: newTodo});
            } catch (err) {
                reject(err.toString());
            }
        });
    }

    updateTodo(ctx){
        return new Promise( async (resolve, reject) => {
            let query = this.query;
            try {
                let result = await query.updateTodo(ctx);
                let updatedTodo = await query.getTodo({id: result.updatedId})
                resolve({status: result.status, updatedTodo: updatedTodo});
            } catch (err) {
                reject(err.toString());
            }
        });
    }

    deleteTodo(ctx){
        return new Promise( async (resolve, reject) => {
            let query = this.query;
            try {
                let result = await query.deleteTodo(ctx);
                resolve(result);
            } catch (err) {
                reject(err.toString());
            }
        });
    }

    importTodo(ctx){
        return new Promise( async (resolve, reject) => {
            let query = this.query;
            try {
                let todos = ctx.todos;
                for(let i in todos){
                    await query.addTodo(todos[i]);
                }
                resolve({status: 'success'});
            } catch (err) {
                reject(err.toString());
            }
        }); 
    }
    
    exportTodo(ctx){
        return new Promise( async (resolve, reject) => {
            let query = this.query;
            try {
                let result = await query.exportTodo(ctx);  
                resolve(result);
            } catch (err) {
                reject(err.toString());
            }
        }); 
    }

    searchTodo(ctx){
        return new Promise( async (resolve, reject) => {
            let query = this.query;
            try {
                let result = await query.searchTodo(ctx);  
                resolve(result);
            } catch (err) {
                reject(err.toString());
            }   
        });
    }

    resetTodo(){
        return new Promise( async (resolve, reject) => {
            let query = this.query;
            try {
                let result = await query.resetTodo();
                resolve('success');
            } catch (err) {
                reject(err.toString());
            }
        });
    }
    
}