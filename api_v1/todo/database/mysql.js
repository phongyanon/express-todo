const config = require('config');
const mysql = require('mysql');

module.exports = class Query {
    constructor(){
        this.con = mysql.createConnection({
            host: config.get('mysql_host'),
            user: config.get('mysql_user'),
            password: config.get('mysql_password'),
            database: config.get('mysql_database')	            
        });
        this.con.connect();
    }

    getAllTodo(){
        return new Promise( (resolve, reject) => {
            let query = `SELECT *, DATE_FORMAT(date, "%d/%m/%Y") as todo_date FROM Todo`;
            this.con.query(query, function(err, result){
                if(err) reject(err.toString());
                resolve(result);
            });
        });
    }

    getTodo(ctx){
        return new Promise( (resolve, reject) => {
            let query = `SELECT *, DATE_FORMAT(date, "%d/%m/%Y") as todo_date FROM Todo Where id = ${ctx.id}`;
            this.con.query(query, function(err, result){
                if(err) reject(err.toString());
                if (result !== undefined){ // to prevent type of id that not a number
                    if(result.length > 0) resolve(result[0]);
                }
                resolve(result);
            });
        });
    }

    addTodo(ctx){
        return new Promise( (resolve, reject) => {
            let query = `INSERT INTO Todo (name, description, date, status) VALUES
                ('${ctx.name}', '${ctx.description}', '${ctx.date}', '${ctx.status}')`;
            this.con.query(query, function(err, result){
                if(err) reject(err.toString());
                try {
                    if(result.affectedRows === 1){
                        resolve({status: "success", newId: result.insertId});
                    } 
                    else reject({error: "addTodo failed"})   
                } catch (err) {
                    reject({error: err.toString()})
                }
            });
        });
    }

    updateTodo(ctx){
        return new Promise( (resolve, reject) => {
            let props = ['name', 'description', 'date', 'status'];
            let update_data = '';
            for (let i in props){
                if(ctx.hasOwnProperty(props[i])) update_data += `${props[i]} = '${ctx[props[i]]}',`;
            }
            update_data = update_data.slice(0, update_data.length - 1); // delete last ','
            let query = `UPDATE Todo SET ${update_data} WHERE id = ${ctx.id}`;
            this.con.query(query, function(err, result){
                if(err) reject(err.toString());
                try {
                    if(result.affectedRows === 1){
                        resolve({status: "success", updatedId: ctx.id});
                    } 
                    else reject({error: "no todo updated."})   
                } catch (err) {
                    reject({error: err.toString()})
                }
            });
        });
    }

    deleteTodo(ctx){
        return new Promise( (resolve, reject) => {
            let query = `DELETE FROM Todo WHERE id = ${ctx.id}`;
            this.con.query(query, function(err, result){
                if(err) reject(err.toString());
                try {
                    if(result.affectedRows === 1){
                        resolve({status: "success", deletedId: ctx.id});
                    }
                    else reject({error: "no todo has been deleted."})   
                } catch (err) {
                    reject({error: err.toString()})
                }
            });
        });
    }

    exportTodo(ctx){
        return new Promise( (resolve, reject) => {
            let query = `SELECT * FROM Todo WHERE date BETWEEN "${ctx.time_from}" AND "${ctx.time_to}" ORDER BY date;`;
            this.con.query(query, function(err, result){
                if(err) reject(err.toString());
                try {
                    resolve({status: "success", todos: result}); 
                } catch (err) {
                    reject({error: err.toString()})
                }
            });
        });
    }

    searchTodo(ctx){
        return new Promise( (resolve, reject) => {
            let query = `SELECT * FROM Todo WHERE name LIKE "%${ctx.searchText}%";`;
            this.con.query(query, function(err, result){
                if(err) reject(err.toString());
                try {
                    resolve({status: "success", todos: result}); 
                } catch (err) {
                    reject({error: err.toString()})
                }
            });
        });  
    }

    resetTodo(){
        return new Promise((resolve, reject) => {
            let query = `TRUNCATE TABLE Todo`;
            this.con.query(query, function(err, result){
                if(err) reject(err.toString());
                resolve(result);
            });
        });
    }
}