const config = require('config');
const mongoose = require('mongoose');
const Todo = require('./mongoModel');

module.exports = class Query{
    constructor(){
        mongoose.connect(config.get('mongo_host'));
        this.db = mongoose.connection;
    }

    getAllTodo(){
        return new Promise((resolve, reject) => {
            Todo.get(function(err, result){
                if(err) reject(err.toString());
                resolve(result);
            });
        });
    }

    getTodo(ctx){
        return new Promise((resolve, reject) => {
            Todo.findById(ctx.id, function(err, result){
                if(err) reject(err.toString());
                resolve(result);
            });
        });
    }

    addTodo(ctx){
        return new Promise((resolve, reject) => {
            let todo = new Todo();
            todo.name = ctx.name;
            todo.description = ctx.description;
            todo.date = ctx.date;
            todo.status = ctx.status;

            todo.save(function(err){
                if(err) reject(err.toString());
                resolve({
                    message: 'New todo created!',
                    data: todo
                });
            });
        });
    }

    updateTodo(ctx){
        return new Promise((resolve, reject) => {
            Todo.findById(ctx.id, function(err, todo){
                if(err) reject(err.toString());
                if(todo === undefined){
                    resolve({message: 'odject not found!'});
                } else {
                    let props = ['name', 'description', 'date', 'status'];
                    for (let i in props){
                        if(ctx.hasOwnProperty(props[i])) todo[props[i]] = ctx[props[i]];
                    }
    
                    todo.save(function(err){
                        if (err) reject(err.toString());
                        resolve({
                            message: 'Update todo!',
                            data: todo
                        })
                    });
                }
            });         
        });
    }

    deleteTodo(ctx){
        return new Promise((resolve, reject) => {
            Todo.deleteOne({_id: ctx.id}, function(err, result){
                if(err) reject(err.toString());
                resolve({
                    status: "success",
                    message: "Todo deleted"
                });
            });
        });
    }

    resetTodo(){
        return new Promise((resolve, reject) => {
            Todo.deleteMany({}, function (err){
                if(err) reject(err.toString());
                resolve(true);
            });
        });
    }
}