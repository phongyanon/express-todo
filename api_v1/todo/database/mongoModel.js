let mongoose = require('mongoose');

let todoSchema = mongoose.Schema({
    name: {
        type: String,
        require: true
    },
    description: String,
    date: {
        type: Date,
        default: Date.now
    },
    status: String
})

let Todo = module.exports = mongoose.model('todo', todoSchema);

module.exports.get = function(callback, limit){
    Todo.find(callback).limit(limit);
}