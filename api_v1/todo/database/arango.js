const config = require('config');
const arango = require('arangojs');
const aql = require('arangojs').aql;

module.exports = class Query {
    constructor(){
        this.db = new arango.Database(config.get('arango_host'));
        this.db.useBasicAuth(config.get('arango_user'), config.get('arango_password'));
        this.db.useDatabase('todo');
    }

    getAllTodo(){
        return new Promise( (resolve, reject) => {
            let query = aql`FOR d IN Todo RETURN d`;
            this.db.query(query).then(
              cursor => cursor.all()
            ).then(
              keys => {resolve(keys)},
              err => {reject(err.toString())}
            );
        });
    }
    getTodo(ctx){
        return new Promise( async (resolve, reject) => {
            let query = `RETURN DOCUMENT('${ctx.id}')`;
            try {
                let cursor = await this.db.query(query);
                let result = await cursor.next();
                resolve(result)
            } catch (err) {
                reject(err.toString());
            }
        });
    }
    addTodo(ctx){
        return new Promise( async (resolve, reject) => {
            let query = aql`INSERT {name: ${ctx.name}, description: ${ctx.description}, 
                date: ${ctx.date}, status: ${ctx.status}} INTO Todo RETURN NEW`;
            try {
                let cursor = await this.db.query(query);
                let result = await cursor.next();
                resolve(result)
            } catch (err) {
                reject(err.toString());
            } 
        });
    }
    updateTodo(ctx){
        return new Promise( async (resolve, reject) => {
            let props = ['name', 'description', 'date', 'status'];
            let update_data = {};
            for (let i in props){
                if(ctx.hasOwnProperty(props[i])) update_data[props[i]] = ctx[props[i]];
            }
            let query = aql`LET d = DOCUMENT(${ctx.id}) UPDATE d._key WITH ${update_data} 
                INTO Todo RETURN NEW`;
            try {
                let cursor = await this.db.query(query);
                let result = await cursor.next();
                resolve(result)
            } catch (err) {
                reject(err.toString());
            }             
        });
    }
    deleteTodo(ctx){
        return new Promise( async (resolve, reject) => {
            let query = aql`LET d = DOCUMENT(${ctx.id})
                REMOVE d._key IN Todo`;
            try {
                let cursor = await this.db.query(query);
                let result = await cursor.next();
                resolve(result)
            } catch (err) {
                reject(err.toString());
            }    
        });
    }
    resetTodo(){
        return new Promise( (resolve, reject) => {
            let query = aql`FOR d IN Todo REMOVE d IN Todo`;
            this.db.query(query).then(
                cursor => cursor.all()
            ).then(
                keys => {resolve(keys)},
                err => {reject(err.toString())}
            );  
        });
    }
}