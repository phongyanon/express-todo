const Todo = require('./todo');

let todo = new Todo();

module.exports = function(app, isLoggedIn) {
    app.get('/api/v1/getAllTodo', isLoggedIn, async (req, res) => {
        try {
            let result = await todo.getAllTodo();
            res.status(200).send({result: result});
        } catch (err) {
            res.status(500).send({error: err.toString()});
        }
    });

    app.get('/api/v1/getTodo', isLoggedIn, async (req, res) => {
        try {
            let result = await todo.getTodo(req.query);
            res.status(200).send({result: result});
        } catch (err) {
            res.status(500).send({error: err.toString()});
        }
    });

    app.post('/api/v1/addTodo', isLoggedIn, async (req, res) => {
        try {
            let result = await todo.addTodo(req.body);
            res.status(200).send({result: result});
        } catch (err) {
            res.status(500).send({error: err.toString()});
        }
    });

    app.post('/api/v1/updateTodo', isLoggedIn, async (req, res) => {
        try {
            let result = await todo.updateTodo(req.body);
            res.status(200).send({result: result});
        } catch (err) {
            res.status(500).send({error: err.toString()});
        }
    });

    app.post('/api/v1/deleteTodo', isLoggedIn, async (req, res) => {
        try {
            let result = await todo.deleteTodo(req.body);
            res.status(200).send({result: result});
        } catch (err) {
            res.status(500).send({error: err.toString()});
        }
    });

    app.post('/api/v1/importTodo', isLoggedIn, async (req, res) => {
        try {
            let result = await todo.importTodo(req.body);
            res.status(200).send({result: result});
        } catch (err) {
            res.status(500).send({error: err.toString()});
        }
    });
    
    app.get('/api/v1/exportTodo', isLoggedIn, async (req, res) => {
        try {
            let result = await todo.exportTodo(req.query);
            res.status(200).send({result: result});
        } catch (err) {
            res.status(500).send({error: err.toString()});
        }
    });

    app.get('/api/v1/searchTodo', isLoggedIn, async (req, res) => {
        try {
            let result = await todo.searchTodo(req.query);
            res.status(200).send({result: result});
        } catch (err) {
            res.status(500).send({error: err.toString()});
        }
    });

    // app.post('/api/v1/resetTodo', isLoggedIn, async (req, res) => {
    //     try {
    //         let result = await todo.resetTodo();
    //         res.status(200).send({result: result});
    //     } catch (err) {
    //         res.status(500).send({error: err.toString()});
    //     }
    // });
}