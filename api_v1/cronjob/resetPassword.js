const Authen = require('../authen/authen');

let authen = new Authen();

let clearResetPasswordToken = function(){
    setInterval( async () => {
        try {
            let all_token = await authen.getAllResetPasswordToken();
            for(i in all_token){
                let expires = all_token[i].expires * 1000;
                if(Date.now() > expires) await authen.deleteResetPasswordToken({id: all_token[i].id})
            }
        } catch (err) {
            console.log(err)
        }
    }, 60 * 60 * 1000); // for 1 hr 
}

module.exports = {
    clearResetPasswordToken
};