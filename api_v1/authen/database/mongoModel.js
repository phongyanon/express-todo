let mongoose = require('mongoose');

let UserSchema = mongoose.Schema({
    username: {
        type: String,
        require: true
    },
    password: {
        type: String,
        require: true
    },
    user_type_id: Number
})

let User = module.exports = mongoose.model('user', UserSchema);

module.exports.get = function(callback, limit){
    User.find(callback).limit(limit);
}