const config = require('config');
const mongoose = require('mongoose');
const User = require('./mongoModel');

const mongojs = require('mongojs'); //TODO: change to use this instead of mongoose
const session_db = mongojs('resthub', ['sessions']);
const bcrypt = require('bcryptjs');

module.exports = class Query{
    constructor(){
        mongoose.connect(config.get('mongo_host'), {useNewUrlParser: true});
        this.db = mongoose.connection;
        this.saltRounds = 10;
    }

    addUser(ctx){
        return new Promise( async (resolve, reject) => {
            let salt = bcrypt.genSaltSync(this.saltRounds);
            let hash = bcrypt.hashSync(ctx.password, salt);
            ctx.password = hash;

            let user = new User();
            user.username = ctx.username;
            user.password = ctx.password;
            user.user_type_id = ctx.user_type_id;

            user.save(function(err){
                if(err) reject(err.toString());
                resolve({
                    message: 'New User created!',
                    data: user
                })
            });
        });
    }

    getUser(ctx){
        return new Promise( async (resolve, reject) => {
            User.findById(ctx.id, function(err, result){
                if(err) reject(err.toString());
                resolve([result]);
            });
        });
    }

    getUserByUsername(ctx){
        return new Promise( async (resolve, reject) => {
            User.findOne({username: ctx.username}, function(err, result){
                if(err) reject(err.toString());
                if (result === null) resolve([]);
                resolve([result]);
            });
        });
    }

    checkUsername(ctx){
        return new Promise( (resolve, reject) => {
            User.findOne({username: ctx.username}, function(err, result){
                if(err) reject(err.toString());
                if (result !== null){
                    resolve(false);
                } else {
                    resolve(true);
                }
            });
        });
    }

    changePassword(ctx){
        return new Promise( async (resolve, reject) => {
            let salt = bcrypt.genSaltSync(this.saltRounds);
            let hash = bcrypt.hashSync(ctx.password, salt); // encode password
            ctx.password = hash;

            User.findById(ctx.id, function(err, user){
                if(err) reject(err.toString());
                user.password = ctx.password;

                user.save(function(err){
                    if(err) reject(err.toString());
                    resolve({
                        status: "success",
                        message: "Password Changed!",
                    })
                });
            });
        });
    }

    deleteUser(ctx){
        return new Promise( async (resolve, reject) => {
            User.deleteOne({_id: ctx.id}, function(err, result){
                if(err) reject(err.toString());
                resolve({
                    status: "success",
                    message: "User deleted!"
                });
            });
        });
    }

    checkPassword(ctx){
        return new Promise( async (resolve, reject) => {
            let result = bcrypt.compareSync(ctx.your_password, ctx.password);
            resolve(result);
        });
    }

    verifySession(ctx){
        return new Promise( async (resolve) => {
            session_db.sessions.findOne({
                _id: ctx.session_id.toString()
            }, function(err, session) {
                if(err) resolve(err.toString());

                let expire = new Date(session.expires * 1000);
                if (Date.now() < expire) resolve(true);
                else resolve(false);
            });
        });
    }
}