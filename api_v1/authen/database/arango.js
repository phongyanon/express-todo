const config = require('config');
const arango = require('arangojs');
const aql = require('arangojs').aql;

const redis_options = {
	port: config.get('redis_port'),
	password: config.get('redis_password')
};
const redis = require("redis"),
		redisDB = redis.createClient(redis_options);
const bcrypt = require('bcryptjs');

module.exports = class Query {
    constructor(){
        this.db = new arango.Database(config.get('arango_host'));
        this.db.useBasicAuth(config.get('arango_user'), config.get('arango_password'));
        this.db.useDatabase('todo');
        this.saltRounds = 10;
    }

    addUser(ctx){
        return new Promise( async (resolve, reject) => {
            let salt = bcrypt.genSaltSync(this.saltRounds);
            let hash = bcrypt.hashSync(ctx.password, salt);
            ctx.password = hash;

            let query = aql`INSERT {username: ${ctx.username}, password: ${ctx.password}, user_type_id: ${ctx.user_type_id}} 
                INTO User RETURN NEW`;
            try {
                let cursor = await this.db.query(query);
                let result = await cursor.next();
                resolve({
                    message: 'New User created!',
                })
            } catch (err) {
                reject(err.toString());
            } 
        });
    }

    getUser(ctx){
        return new Promise( async (resolve, reject) => {
            let query = `RETURN DOCUMENT('${ctx.id}')`;
            try {
                let cursor = await this.db.query(query);
                let result = await cursor.next();
                resolve([result])
            } catch (err) {
                reject(err.toString());
            }
        });
    }

    getUserByUsername(ctx){
        return new Promise( (resolve, reject) => {
            let query = aql`FOR d IN User FILTER d.username == ${ctx.username} RETURN d`;
            this.db.query(query).then(
                cursor => cursor.all()
            ).then(
                keys => {resolve(keys)},
                err => {reject(err.toString())}
            );
        });
    }

    checkUsername(ctx){
        return new Promise( (resolve, reject) => {
            let query = aql`FOR d IN User FILTER d.username == ${ctx.username} RETURN d`;
            this.db.query(query).then(
                cursor => cursor.all()
            ).then(
                keys => {resolve(keys !== null)},
                err => {reject(err.toString())}
            );
        });
    }

    changePassword(ctx){
        return new Promise( async (resolve, reject) => {
            let salt = bcrypt.genSaltSync(this.saltRounds);
            let hash = bcrypt.hashSync(ctx.password, salt); // encode password
            ctx.password = hash;

            let query = aql`LET d = DOCUMENT(${ctx.id})
                UPDATE d._key WITH {password: ${ctx.password}} 
                INTO User`;
            try {
                let cursor = await this.db.query(query);
                let result = await cursor.next();
                resolve({
                    status: "success",
                    message: "Password Changed!",
                })
            } catch (err) {
                reject(err.toString());
            }  
        });
    }

    deleteUser(ctx){
        return new Promise( async (resolve, reject) => {
            let query = aql`LET d = DOCUMENT(${ctx.id})
                REMOVE d._key IN User`;
            try {
                let cursor = await this.db.query(query);
                let result = await cursor.next();
                resolve({
                    status: "success",
                    message: "User deleted!"
                });
            } catch (err) {
                reject(err.toString());
            } 
        });
    }

    checkPassword(ctx){
        return new Promise( (resolve, reject) => {
            let result = bcrypt.compareSync(ctx.your_password, ctx.password);
            resolve(result);
        });
    }

    verifySession(ctx){
        return new Promise( async (resolve, reject) => {
            // TODO: query session if get session and not expire return true. 
            console.log(ctx.session_id);
            redisDB.get(`sessions:${ctx.session_id}`, function(err, value){
                if(err) reject(err.toString());
        
                let session = JSON.parse(value);
                let expire = new Date(session.cookie.expires);
                if (Date.now() < expire) resolve(true);
                else resolve(false);
            });
        });
    }
}
