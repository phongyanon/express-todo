const config = require('config');
const mysql = require('mysql');
const bcrypt = require('bcryptjs');
const uuidv5 = require('uuid/v5');
const uuidv4 = require('uuid/v4');

module.exports = class Query {
    constructor(){
        this.con = mysql.createConnection({
            host: config.get('mysql_host'),
            user: config.get('mysql_user'),
            password: config.get('mysql_password'),
            database: config.get('mysql_database')	            
        });
        this.con.connect();
        this.saltRounds = 10;
    }

    addUser(ctx){
        return new Promise( async (resolve, reject) => {
            let salt = bcrypt.genSaltSync(this.saltRounds);
            let hash = bcrypt.hashSync(ctx.password, salt);
            ctx.password = hash;
    
            let query = `INSERT INTO User (username, password, user_type_id) VALUES 
                ('${ctx.username}', '${ctx.password}', '${ctx.user_type_id}')`;
            this.con.query(query, function(err, result){
                if (err) reject(err.toString);
                resolve({
                    message: 'New User created!',
                    data: result
                });
            });
        });
    }

    getUser(ctx){
        return new Promise( (resolve, reject) => {
            let query = `SELECT * FROM User WHERE id = '${ctx.id}'`;
            this.con.query(query, function(err, result){
                if (err) reject(err.toString());
                resolve(result);
            });
        });
    }
    
    getUserByUsername(ctx){
        return new Promise( (resolve, reject) => {
            let query = `SELECT * FROM User WHERE username = '${ctx.username}'`;
            this.con.query(query, function(err, result){
                if (err) reject(err.toString());
                resolve(result);
            });
        });
    }

    checkUsername(ctx){
        return new Promise( (resolve, reject) => {
            let query = `SELECT * FROM User WHERE username = '${ctx.username}'`;
            this.con.query(query, function(err, result){
                if (err) reject(err.toString());
                if (result.length > 0) {
                    resolve(false); // this username have been used already.
                } else {
                    resolve(true);
                }
            });
        });
    }

    changePassword(ctx){
        return new Promise( async (resolve, reject) => {
            let salt = bcrypt.genSaltSync(this.saltRounds);
            let hash = bcrypt.hashSync(ctx.password, salt); // encode password
            ctx.password = hash;

            let query = `UPDATE User SET password = '${ctx.password}' WHERE id = '${ctx.id}'`;
            this.con.query(query, function(err, result){
                if (err) reject(err.toString());
                try {
                    if(result.affectedRows === 1){
                        resolve({status: "success", updatedId: ctx.id});
                    } 
                    else reject({error: "no todo updated."})   
                } catch (err) {
                    reject({error: err.toString()})
                }
            });
        });
    }

    deleteUser(ctx){
        return new Promise( (resolve, reject) => {
            let query = `DELETE FROM User WHERE id = '${ctx.id}'`;
            this.con.query(query, function(err, result){
                if (err) reject(err.toString());
                resolve({
                    status: "success",
                    message: "User deleted!",
                    data: result
                });
            });
        });
    }
    
    checkPassword(ctx) {
        return new Promise( async (resolve, reject) => {
            let result = bcrypt.compareSync(ctx.your_password, ctx.password);
            resolve(result);
        });
    }

    verifySession(ctx){
        return new Promise( async (resolve, reject) => {
            let query = `SELECT * FROM sessions WHERE session_id = '${ctx.session_id}' LIMIT 1`;
            this.con.query(query, function(err, result){
                if (err) reject(err.toString());
                
                if(result.length !== 0){
                    let session = result[0];
                    let expire = new Date(session.expires * 1000);

                    if (Date.now() < expire) resolve(true);
                    else resolve(false);
                    
                } else resolve(false);
            });
        });
    }

    checkResetPasswordToken(ctx){
        return new Promise( (resolve, reject) => {
            let query = `SELECT * FROM ResetPasswordToken WHERE token = '${ctx.token}' LIMIT 1`;
            this.con.query(query, function(err, result){
                if (err) reject(err.toString());
                
                if(result.length !== 0){
                    if (Date.now() < result[0].expires * 1000) resolve(true);
                    else resolve(false);
                    
                } else resolve(false);
            });
        });
    }

    addResetPasswordToken(ctx){
        return new Promise( (resolve, reject) => {
            let token = uuidv5(ctx.user.username, uuidv4());
            let expires_time = (Date.now() + 15 * 60 * 1000) / 1000; // now + 15 minutes
            let query = `INSERT INTO ResetPasswordToken (token, user_id, expires) VALUES 
                ('${token}', '${ctx.user.id}', ${expires_time})`;
            this.con.query(query, function(err, result){
                if (err) reject(err.toString());
                resolve({
                    message: 'New reset password token created!',
                    data: {status: "success", newId: result.insertId}
                });
            });
        });
    }

    getAllResetPasswordToken(){
        return new Promise( (resolve, reject) => {
            let query = `SELECT * FROM ResetPasswordToken`;
            this.con.query(query, function(err, result){
                if (err) reject(err.toString());
                resolve(result);
            });
        }); 
    }

    getResetPasswordToken(ctx){
        return new Promise( (resolve, reject) => {
            let query = `SELECT * FROM ResetPasswordToken WHERE id = '${ctx.id}'`;
            this.con.query(query, function(err, result){
                if (err) reject(err.toString());
                if (result !== undefined){ // to prevent type of id that not a number
                    if(result.length > 0) resolve(result[0]);
                }
                resolve(result);
            });
        });
    }

    getResetPasswordTokenByToken(ctx){
        return new Promise( (resolve, reject) => {
            let query = `SELECT * FROM ResetPasswordToken WHERE token = '${ctx.token}'`;
            this.con.query(query, function(err, result){
                if (err) reject(err.toString());
                if (result !== undefined){ // to prevent type of id that not a number
                    if(result.length > 0) resolve(result[0]);
                }
                resolve(result);
            });
        });
    }

    deleteResetPasswordToken(ctx){
        return new Promise( (resolve, reject) => {
            let query = `DELETE FROM ResetPasswordToken WHERE id = '${ctx.id}'`;
            this.con.query(query, function(err, result){
                if (err) reject(err.toString());
                resolve({
                    status: "success",
                    message: "ResetPasswordToken deleted!",
                    data: result
                });
            });
        });
    }
}