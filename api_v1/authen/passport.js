const LocalStrategy   = require('passport-local').Strategy;
const Authen = require('./authen');
const config = require('config');
const util = require('../../spec/helpers/utils');
let authen = new Authen();

module.exports = function(passport) {
    // used to serialize the user for the session
    passport.serializeUser(function(user, done) {
        // console.log('serialize: ', user)
        let t_id = util.getIdByDB(config.get('db_type'), user);
        done(null, t_id);
    });

    // used to deserialize the user
    passport.deserializeUser(async function(id, done) {
        // console.log('deserialize: ', id)        
        try {
            let user = await authen.getUser({'id': id});
            done(null, {'username': user.username});
        } catch (err) {
            done(null, false);
        }
    });

    // we are using named strategies since we have one for login and one for signup
	// by default, if there was no name, it would just be called 'local'

    passport.use('local-signup', new LocalStrategy({
        // by default, local strategy uses username and password
        usernameField : 'username',
        passwordField : 'password',
        passReqToCallback : true // allows us to pass back the entire request to the callback
    },
    async function(req, username, password, done) {
        try {
            let user_type_id = req.body.user_type_id;

            let user_result = await authen.getUserByUsername({'username': username});
            if (user_result !== false) { // if there's already this username
                return done(null, false);
            } else { // if there's no one use this username
                try {
                    let add_result = await authen.addUser({'username': username, 'password': password, 'user_type_id': user_type_id});
                    let new_user = await authen.getUserByUsername({'username': username});
                    return done(null, new_user);
                } catch (err) {
                    throw err;
                }
            }
        } catch (err) {
            return err.toString();
        }
    }));

    // we are using named strategies since we have one for login and one for signup
    // by default, if there was no name, it would just be called 'local'

    passport.use('local-login', new LocalStrategy({
        // by default, local strategy uses username and password
        usernameField : 'username',
        passwordField : 'password',
        passReqToCallback : true // allows us to pass back the entire request to the callback
    },
    async function(req, username, password, done) { // callback with email and password from our form
        try {
            let result = await authen.login({'username': username, 'password': password});
            let user = await authen.getUserByUsername({'username': username});
            if (result === true){
                return done(null, user);
            } else {
                return done(null, false);
            }
        } catch (err) {
            return err.toString();
        }
    }));

};