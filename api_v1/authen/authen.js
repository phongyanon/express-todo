const config = require('config');
const emailAction = require('../utils/sendMail');
module.exports = class Authen {
    constructor() {
        let Query;
        switch(config.get('db_type')){
            case('mysql'):
                Query = require('./database/mysql');
                break;
            case('mongo'):
                Query = require('./database/mongo');
                break;
            case('arango'):
                Query = require('./database/arango');
                break;
        }
        
        this.query = new Query();
    }
    register(ctx){
        return new Promise( async (resolve, reject) => {
            let query = this.query;
            try {
                let check_username = await query.checkUsername({'username': ctx.username});
                if (check_username === true) {
                    let result = await query.addUser(ctx);
                    resolve(result);
                } else {
                    resolve(false);  // 'This username have been used already.'
                }      
            } catch (err) {
                reject(err.toString());
            }
        });
    }
    login(ctx){
        return new Promise( async (resolve, reject) => {
            let query = this.query;
            try {
                // get username then check password
                let user_result = await query.getUserByUsername(ctx);
                if (user_result.length === 0) resolve(false);
                let user = user_result[0];

                let result = await query.checkPassword({'your_password':ctx.password, 'password': user.password});
                if (result === true){
                    resolve(true);
                } else {
                    resolve(false);
                }
            } catch (err) {
                reject('login: ' + err.toString());
            }
        });
    }    
    getUser(ctx){
        return new Promise( async (resolve, reject) => {
            let query = this.query;
            try {
                let user_result = await query.getUser(ctx);
                if (user_result.length > 0){
                    resolve(user_result[0]);
                } else {
                    resolve(false);
                }
            } catch (err) {
                reject(err.toString());
            }
        });
    }
    getUserByUsername(ctx){
        return new Promise( async (resolve, reject) => {
            let query = this.query;
            try {
                let user_result = await query.getUserByUsername(ctx);
                if (user_result.length > 0){
                    resolve(user_result[0]);
                } else {
                    resolve(false);
                }
            } catch (err) {
                reject(err.toString());
            }
        });
    }
    addUser(ctx){
        return new Promise( async (resolve, reject) => {
            let query = this.query;
            try {
                let result = await query.addUser(ctx);
                resolve(result);
            } catch (err) {
                reject(err.toString());
            }
        });
    }
    deleteUser(ctx){
        return new Promise( async (resolve, reject) => {
            let query = this.query;
            try {
                let result = await query.deleteUser(ctx);
                resolve(result);
            } catch (err) {
                reject(err.toString());
            }
        });        
    }
    verifySession(ctx){
        return new Promise( async (resolve, reject) => {
            let query = this.query;
            try {
                let result = await query.verifySession(ctx);
                resolve(result);
            } catch (err) {
                reject(err.toString());
            }
        });
    }
    resetPassword(ctx){
        return new Promise( async (resolve, reject) => {
            let query = this.query;
            try {
                let result_token = await query.checkResetPasswordToken(ctx);
                if(result_token){
                    let token = await query.getResetPasswordTokenByToken(ctx);
                    ctx['id'] = token.user_id;
                    let result = await query.changePassword(ctx);
                    resolve(result)
                } else {
                    resolve({error: true, msg: 'Reset password token expires.'})
                }
            } catch (err) {
                reject(err.toString());
            }
        });
    }
    forgetPassword(ctx){
        return new Promise( async (resolve, reject) => {
            let query = this.query;
            try {
                let user_result = await query.getUserByUsername(ctx);
                if (user_result.length > 0){
                    ctx['user'] = user_result[0];
                    let result = await query.addResetPasswordToken(ctx);
                    if(result.message === 'New reset password token created!'){
                        
                        let resetPasswordUrl = `http://localhost:8000/resetPassword`;
                        if(config.get('config_id') === 'development') resetPasswordUrl = 'http://localhost:8080/resetPassword';
                        let resetPasswordToken = await query.getResetPasswordToken({id: result.data.newId});
                        
                        let result_email = await emailAction.sendMail({
                            email_to: ctx.user.username,
                            subject: 'Todo platform reset password',
                            text: `For Todo platform your url to reset password is ${resetPasswordUrl}/${resetPasswordToken.token}.`
                        })
                        
                        if(result_email.success) resolve({error: false, msg: 'success'});
                        else resolve({error: true, msg: 'Please check your email to reset password.'})
                    } else resolve({error: true, msg: 'Can not create reset password token.'})
                } else {
                    resolve({error: 'Username not found.'});
                }
            } catch (err) {
                reject(err.toString());
            }
        });
    }
    verifyResetPasswordToken(ctx){
        return new Promise( async (resolve, reject) => {
            let query = this.query;
            try {
                let result_token = await query.checkResetPasswordToken(ctx);
                if(result_token){
                    resolve({status: 'success', msg: result_token});
                } else {
                    resolve({status: 'failed', msg: 'Reset password token expires.'})
                }
            } catch (err) {
                reject(err.toString());
            }
        });
    }

    getAllResetPasswordToken(){
        return new Promise( async (resolve, reject) => {
            let query = this.query;
            try {
                let result = await query.getAllResetPasswordToken();
                resolve(result);
            } catch (err) {
                reject(err.toString());
            }
        });
    }

    deleteResetPasswordToken(ctx){
        return new Promise( async (resolve, reject) => {
            let query = this.query;
            try {
                let result = await query.deleteResetPasswordToken(ctx);
                resolve(result);
            } catch (err) {
                reject(err.toString());
            }
        });      
    }
}