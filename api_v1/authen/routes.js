const Authen = require('./authen');

let authen = new Authen();

module.exports = function(app, passport, isLoggedIn) {
	app.post('/api/v1/login', passport.authenticate('local-login'), function(req, res, info) {
		// console.log('req.user: ', req.user);

		res.status(200).json({
			'username': req.user.username,
			'user_id': req.user.id,
			'session_id': req.sessionID,
			'expires': req.session.cookie._expires
		});
	});

	app.get('/todo', function(req, res){
		res.status(200).send('Overview Page');
	});

	app.get('/api/v1/isSignIn', isLoggedIn, function(req, res){
		res.status(200).send({result: true, msg: 'Successfully sign in.'});
	});

	app.post('/api/v1/resetPassword', async function(req, res){
		try{
			let result = await authen.resetPassword(req.body);
			res.status(200).send({"result": result});
		} catch(err){
			res.status(500).send({"error": err.toString()});			
		}
	});

	app.post('/api/v1/forgetPassword', async function(req, res){
		try{
			let result = await authen.forgetPassword(req.body);
			res.status(200).send({"result": result});
		} catch(err){
			res.status(500).send({"error": err.toString()});			
		}
	});

	app.get('/api/v1/verifyResetPasswordToken', async function(req, res){
        try {
            let result = await authen.verifyResetPasswordToken(req.query);
            res.status(200).send({result: result});
        } catch (err) {
            res.status(500).send({error: err.toString()});
        }
	});

	// process the signup form
	// app.post('/api/v1/signup', passport.authenticate('local-signup', {
	// 	successRedirect : '/todo', // redirect to the secure profile section
	// 	failureRedirect : '/', // redirect back to the signup page if there is an error
	// 	failureFlash : true // allow flash messages
	// }));
	// example success with not redirect but use function instead
	app.post('/api/v1/signup', passport.authenticate('local-signup'), function(req, res){
		res.send({msg: 'success'});
	});

	app.get('/api/v1/logout', function(req, res) {
		req.logout();
		res.status(200).send({result: 'Successfully sign out.'});
	});

	app.get('/api/v1/getUserType', isLoggedIn, async (req, res) => {
		try{
			let result = await authen.getUserType(req.user);
			res.status(200).send({"result": result});
		} catch(err){
			res.status(500).send({"error": err.toString()});			
		}
	});

	app.get('/api/v1/verify/session', async function(req, res){
		try{
			let result = await authen.verifySession(req.query);
			res.status(200).send({"result": result});
		} catch(err){
			res.status(500).send({"error": err.toString()});			
		}
	});


};
