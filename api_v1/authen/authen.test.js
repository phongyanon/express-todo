const query = require('./database/mysql')

let obj = new query()

test('Function verify correct password', async () => {
    try {
        ctx = {
            your_password: 'password',
            password: '$2y$10$tXUAQGhRpbmFLfbBrStOAO67uKqiuw442nn2H3/yuUCCys0KEg/lC'
        }
        let result = await obj.checkPassword(ctx)
        expect(result).toBe(true)
    } catch (err) {
        console.log(err)
    }
})

test('Function verify wrong password', async () => {
    try {
        ctx2 = {
            your_password: 'helloworld',
            password: '$2y$10$tXUAQGhRpbmFLfbBrStOAO67uKqiuw442nn2H3/yuUCCys0KEg/lC'
        }
        let result2 = await obj.checkPassword(ctx2)
        expect(result2).toBe(false)
    } catch (err) {
        console.log(err)
    }
})