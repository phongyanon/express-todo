let assert = require('assert');
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../../index');
let should = chai.should();
chai.use(chaiHttp);
const Authen = require('../../api_v1/authen/authen');
const config = require('config');
let authen = new Authen();
let db_type = config.get('db_type');
const utils = require('../helpers/utils');

describe('Authen api', function(){
    describe('CRUD Authen api', function(){
        let user = {
            "username": "kaew",
            "password": "1234",
            "user_type_id": 1
        }
        let result_user;
        it('create new user', async function() {
            let result = await authen.addUser(user);
            expect(result.message).to.equal('New User created!');
            // console.log('new user: ', result);
        });
        it('get user by username', async function() {
            result_user = await authen.getUserByUsername({username: 'kaew'});
            expect(result_user).to.have.property('username');
            expect(result_user.username).to.equal('kaew');
            // console.log('Get user by username: ', result_user);
        });
        it('get user by id', async function() {
            let t_id = utils.getIdByDB(db_type, result_user);
            let result = await authen.getUser({id: t_id});
            // console.log('Get user by id: ', result);

            expect(result).to.have.property('username');
            expect(result.username).to.equal('kaew');
        });
        it('delete user', async function() {
            let t_id = utils.getIdByDB(db_type, result_user);
            let result = await authen.deleteUser({id: t_id});
            // console.log('Delete User: ', result);
            expect(result.status).to.equal('success');
        });
    });
});