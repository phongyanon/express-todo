let assert = require('assert');
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../../index');
let should = chai.should();
chai.use(chaiHttp);
const Todo = require('../../api_v1/todo/todo');
const config = require('config');
let todo = new Todo();
let db_type = config.get('db_type');
const utils = require('../helpers/utils');

describe("Todos", function(){
    describe("DELETE All", function(){
        it("should remove all first",  (done) => {
            todo.resetTodo().then((result) => {
                expect(result).to.equal('success');
                done();   
            }).catch((err) => {
                done();
            });
        });
    });

    describe("CRUD operation", function(){
        let todos = [{
           name: "chai",
           description: "assertion library",
           date: "2019-04-1",
           status: "active"
        },{
           name: "macha",
           description: "test framework",
           date: "2019-04-2",
           status: "active"
        }]
        let result_todos;
        
        it("add todo to DB", (done) => {
            for (i in todos){
                chai.request(server)
                    .post('/api/v1/addTodo')
                    .send(todos[i])
                    .end((err, res) => {
                        res.should.have.status(200);
                        // console.log("Response Body: ", res.body);
                    });
            }
            done();
        });

        it("get all todos", (done) => {
            chai.request(server)
                .get('/api/v1/getAllTodo')
                .end((err, result) => {
                    result.should.have.status(200);
                    result_todos = result.body.result;
                    expect(result_todos.length).to.equal(2);
                    for( i in result_todos){
                        expect(result_todos[i]).to.have.property('name');
                        expect(result_todos[i]).to.have.property('description');
                        expect(result_todos[i]).to.have.property('date');
                        expect(result_todos[i]).to.have.property('status');
                    }
                    // console.log("Got", result.body.result.length, " docs");

                    done();
                });
        });
        
        it("Fetch particular Todo only", (done) => {
            let t_id = utils.getIdByDB(db_type, result_todos[0]);
            chai.request(server)
                .get('/api/v1/getTodo')
                .query({id: t_id})
                .end((err, result) => {
                    result.should.have.status(200);
                    result = result.body.result;
                    expect(result).to.have.property('name');
                    expect(result).to.have.property('description');
                    expect(result).to.have.property('date');
                    expect(result).to.have.property('status');
                    if(result.name === 'chai') {
                        expect(result.name).to.equal('chai');
                        expect(result.description).to.equal('assertion library');
                    }
                    if(result.name === 'mocha') {
                        expect(result.name).to.equal('mocha');
                        expect(result.description).to.equal('test framework');
                    }
           
                    // console.log('fetch particular todo: ', result.body);
                    done();
                });
        });

        it("Update particular Todo only", (done) => {
            let t_id = utils.getIdByDB(db_type, result_todos[0]);
            chai.request(server)
                .post('/api/v1/updateTodo')
                .send({
                    "id": t_id,
                    "name": "ESP32",
                    "description": "multiple regression",
                    "date": "2019-03-24",
                    "status": "complete"
                }).end((err, result) => {
                    result.should.have.status(200);
                    done();
                });
        });

        it("check Todo update in DB", (done) => {
            let t_id = utils.getIdByDB(db_type, result_todos[0]);
            chai.request(server)
                .get('/api/v1/getTodo')
                .query({id: t_id})
                .end((err, result) => {
                    result.should.have.status(200);
                    result = result.body.result;
                    expect(result).to.have.property('name');
                    expect(result).to.have.property('description');
                    expect(result).to.have.property('date');
                    expect(result).to.have.property('status');
                    
                    expect(result.name).to.equal('ESP32');
                    expect(result.description).to.equal('multiple regression');
                    expect(result.status).to.equal('complete');
                    // console.log('fetch particular todo: ', result.body);
                    done();
                });
        });

        it("Delete particular Todo", (done) => {
            let t_id = utils.getIdByDB(db_type, result_todos[0]);
            chai.request(server)
                .post('/api/v1/deleteTodo')
                .send({id: t_id})
                .end((err, result) => {
                    result.should.have.status(200);
                    // console.log('Delete particular Todo: ', result.body);
                    done();
                });
        });

        it("Confirm delete Todo", (done) => {
            chai.request(server)
                .get('/api/v1/getAllTodo')
                .end((err, result) => {
                    result.should.have.status(200);
                    result.body.result.length.should.eq(1);
                    // console.log("Got ", result.body.result[0].length, " Docs");
                    done();            
                });
        });
    });
});