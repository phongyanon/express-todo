let assert = require('assert');
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../../index');
let should = chai.should();
chai.use(chaiHttp);
const Authen = require('../../api_v1/authen/authen');
const config = require('config');
let authen = new Authen();
let db_type = config.get('db_type');
const utils = require('../helpers/utils');

describe('Authen', function(){
    describe('Session Authen', function(){
        let user = {
                "username": "orn",
                "password": "1234",
                "user_type_id": 1
            }
        it('Sign up', (done) => {
            chai.request(server)
                .post('/api/v1/signup')
                .send(user)
                .end( async (err, res) => {
                    res.should.have.status(200);
                    done();
                });
        });
        it('Sign up again', (done) => {
            chai.request(server)
                .post('/api/v1/signup')
                .send(user)
                .end( async (err, res) => {
                    res.should.have.status(200);
                    done();
                });
        });
        it('Sign in and check is signed in and sign out.', (done) => {
            chai.request(server)
                .post('/api/v1/login')
                .send({
                    username: 'orn',
                    password: '1234'
                }).end((err, res) => {
                    res.should.have.status(200);
                    let result = res.body;
                    expect(result).to.have.property('username');
                    expect(result).to.have.property('session_id');
                    let session_id = result.session_id;

                    chai.request(server)
                    .get('/api/v1/verify/session')
                    .query({session_id: session_id})
                    .end((err, res) => {
                        res.should.have.status(200);
                        let result = res.body;
                        expect(result).to.have.property('result');
                        expect(result.result).to.equal(true);
                        
                        chai.request(server) // checkout signed out
                        .get('/api/v1/logout')
                        .end((err, res) => {
                            res.should.have.status(200);
                            expect(res.body).to.have.property('result');
                            expect(res.body.result).to.equal('Successfully sign out.');
                            done();
                        });
                    });
                });
        });
    });
});