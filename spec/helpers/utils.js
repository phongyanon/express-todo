module.exports = {
    getIdByDB: function(database, data){
        switch(database){
            case('mysql'):
                return data.id;
            case('mongo'):
                return data._id;
            case('arango'):
                return data._id;
                break;
        }
    }
}