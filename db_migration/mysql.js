// script to create all Table in database
CREATE TABLE `todo`.`User` ( 
    `id` INT(255) NOT NULL AUTO_INCREMENT , 
    `username` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL , 
    `password` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL , 
    `user_type_id` INT NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;