const config = require('config');
const arango = require('arangojs');

let db = new arango.Database(config.get('arango_host'));
db.useBasicAuth(config.get('arango_user'), config.get('arango_password'));

db.createDatabase('todo', function(err){
    if(!err) console.log('Database todo created by arango');
    else console.error('Failed to create database:', err);
});