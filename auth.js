const config = require('config');
const passport = require('passport');
const randomstring = require("randomstring");
const session = require('express-session');

require('./api_v1/authen/passport')(passport);

module.exports = function(app){
    let db_type = config.get('db_type');
    let sessionStore;
    switch(db_type){
        case ('mysql'):
            const MySQLStore = require('express-mysql-session')(session);

            let options = {
                host: config.get('mysql_host'),
                user: config.get('mysql_user'),
                password: config.get('mysql_password'),
                database: config.get('mysql_database'),
                createDatabaseTable: true
                // port: 3306
            };
            
            sessionStore = new MySQLStore(options);
            break;
        case ('mongo'):
            const MongoStore = require('connect-mongo')(session);

            let mongo_options = {
                url: config.get('mongo_host'),
                autoRemove: 'interval',
                autoRemoveInterval: 10 // In minutes. Default
            }
            sessionStore = new MongoStore(mongo_options);         
            break;
        default: // use redis
            const RedisStore = require('connect-redis')(session);
        
            let redis_options = {
                url: config.get('redis_host'),
                pass: config.get('redis_password'),
                prefix: 'sessions:',
                ttl: 86400
            };
            sessionStore = new RedisStore(redis_options);
            break;
    }
    
    // required for passport
    app.use(session({
        secret: randomstring.generate(12),
        httpOnly: false,
        store: sessionStore,
        resave: false,
        saveUninitialized: true,
        cookie: { maxAge: 720000 }
    }));
    app.use(passport.initialize());
    app.use(passport.session()); // persistent login sessions
}