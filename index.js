const express = require('express');
const app = express();
const config = require('config');
const port = config.get('http_port');
const cors = require('cors');
const path = require('path');

const passport = require('passport');
const bodyParser = require('body-parser');

const resetPassword_cron = require('./api_v1/cronjob/resetPassword');

const corsOptions = {
    origin: '*',
    method: "GET,HEAD,PUT,PATCH,POST,DELETE",
    header:{
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',  
    }
}

app.use(cors(corsOptions));

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname+'/public')));

const env_dev = config.get('config_id') !== 'development' ? true: false;
// if(env_dev) require('./auth')(app);
require('./auth')(app); // for react development


// route middleware to make sure
let isLoggedIn = function (req, res, next) {
    // if user is authenticated in the session, carry on
    // console.log('isLoggedIn: ', req.isAuthenticated());
    // TODO: use req.user to get user_type_id for api permission
    // TODO: redis or other database should use strong password such generate from command  openssl rand 60 | openssl base64 -A.
    if(env_dev){
        console.log('isLoggedIn: ', req.isAuthenticated(), req.user)
        if (req.isAuthenticated())
            return next();
    
        res.status(401).send({
            status: 'Unauthorized',
            message: 'Please go to login page'
        });
    } else {
        return next();
    }
}

require('./api_v1/authen/routes')(app, passport, isLoggedIn);
require('./api_v1/todo/routes')(app, isLoggedIn);

app.get('/', function(req, res){
    // console.log(req.session);
    try {
        if(req.session.page_views){
            req.session.page_views++;
            res.status(200).send("You visited this page " + req.session.page_views + " times");
         } else {
            req.session.page_views = 1;
            res.status(200).send("Welcome to this page for the first time!");
         }   
    } catch (err) {
        res.status(200).send("Running in development without authorization.");
    }
 });

 app.get('*',(req,res)=>{
    res.sendFile(path.join(__dirname+'/public/index.html'));
})

// launch cron job
resetPassword_cron.clearResetPasswordToken();

// launch ======================================================================
app.listen(port, () => {
    console.log('This server is running on port: ' + port);
});
module.exports = app;